#!/usr/bin/env python

"""
A simple echo client
"""

import socket
import sys
import subprocess


serverpubkey=("10142701089716483","5")
clientprivkey=("10142789312725007","8114231289041741")
clientpubkey=("10142789312725007",5)

host = 'localhost'
port = 8080
size = 1024
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((host,port))
msg=sys.argv[1]
encrypt=subprocess.check_output(["./encrypt",serverpubkey[0],serverpubkey[1],msg])
s.send(encrypt)
data = s.recv(size)
decrypt=subprocess.check_output(["./decrypt",clientprivkey[0],clientprivkey[1],data])
print data,decrypt
s.close()
