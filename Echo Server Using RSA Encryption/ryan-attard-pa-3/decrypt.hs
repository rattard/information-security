import System.Environment
main = do
    args <- getArgs
    putStrLn $ show  (argtoint(args))
argtoint a = do
        decrypt (read (head a) :: Integer) (read (head (tail a)) :: Integer) (read (head (tail (tail a))) :: Integer)
decrypt n d c = expmod n d c

expmod n d c = let
                 exfun a d s | d==0 = s
                          | d `mod ` 2 == 0 = ((exfun (a*a `mod` n)) (d `div` 2)) s
                          | otherwise = ((exfun (a*a `mod` n)) (d `div` 2)) (s*a `mod` n)
                in exfun c d 1
