Swann's Way by Marcel Proust
http://www.gutenberg.org/cache/epub/7178/pg7178.txt
MD5(swann.txt)= fcdfb168e9379381cca76986af4f651c
SHA1(swann.txt)= 67c3490b649dff805a19e5ba01306f30c228eea7

OpenSSL 1.0.1 14 Mar 2012 built on: Tue Aug 21 05:18:48 UTC 2012 platform: debian-amd64 options: bn(64,64) rc4(16x,int) des(idx,cisc,16,int) blowfish(idx) compiler: cc -fPIC -DOPENSSL_PIC -DZLIB -DOPENSSL_THREADS -D_REENTRANT -DDSO_DLFCN -DHAVE_DLFCN_H -m64 -DL_ENDIAN -DTERMIO -g -O2 -fstack-protector --param=ssp-buffer-size=4 -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -Wl,-Bsymbolic-functions -Wl,-z,relro -Wa,--noexecstack -Wall -DOPENSSL_NO_TLS1_2_CLIENT -DOPENSSL_MAX_TLS1_2_CIPHER_LENGTH=50 -DMD32_REG_T=int -DOPENSSL_IA32_SSE2 -DOPENSSL_BN_ASM_MONT -DOPENSSL_BN_ASM_MONT5 -DOPENSSL_BN_ASM_GF2m -DSHA1_ASM -DSHA256_ASM -DSHA512_ASM -DMD5_ASM -DAES_ASM -DVPAES_ASM -DBSAES_ASM -DWHIRLPOOL_ASM -DGHASH_ASM OPENSSLDIR: "/usr/lib/ssl"

Commands to digest files files
echo $(openssl dgst -md5 swann.txt) >>output.txt
echo $(openssl dgst -sha1 swann.txt) >>output.txt
echo $(openssl version -a) >>output.txt
