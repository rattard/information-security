
dhcp21:Downloads byron$ openssl s_client -connect smtp.googlemail.com:465 -crlf
CONNECTED(00000003)
depth=1 C = US, O = Google Inc, CN = Google Internet Authority
verify error:num=20:unable to get local issuer certificate
verify return:0
---
Certificate chain
 0 s:/C=US/ST=California/L=Mountain View/O=Google Inc/CN=smtp.googlemail.com
   i:/C=US/O=Google Inc/CN=Google Internet Authority
 1 s:/C=US/O=Google Inc/CN=Google Internet Authority
   i:/C=US/O=Equifax/OU=Equifax Secure Certificate Authority
---
Server certificate
-----BEGIN CERTIFICATE-----
MIIDYDCCAsmgAwIBAgIKIZtavQADAAA/jDANBgkqhkiG9w0BAQUFADBGMQswCQYD
VQQGEwJVUzETMBEGA1UEChMKR29vZ2xlIEluYzEiMCAGA1UEAxMZR29vZ2xlIElu
dGVybmV0IEF1dGhvcml0eTAeFw0xMjAxMDQyMzMxNDhaFw0xMzAxMDQyMzQxNDha
MG0xCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpDYWxpZm9ybmlhMRYwFAYDVQQHEw1N
b3VudGFpbiBWaWV3MRMwEQYDVQQKEwpHb29nbGUgSW5jMRwwGgYDVQQDExNzbXRw
Lmdvb2dsZW1haWwuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDlTm3l
KVXruQdpTQWdx+qGkpdvbpCOoPDO8Z4x3Yv7Tn5J02VKwC8zWsd6PLRuctaWR3Xw
6umeVsN1VPoEnWkWCwu6ZLth1gC1pUSUcwMrRsj7ibZTy2uz2tGO+EYWEEPnGeMM
rY0o+RhXEWiF40JXiQmvCku1odi2HNBO0/RzIwIDAQABo4IBLDCCASgwHQYDVR0O
BBYEFHAfKmoNs2k50Rt25uxjd+uN3h9JMB8GA1UdIwQYMBaAFL/AMOv1QxE+Z7qe
kfv8atrjaxIkMFsGA1UdHwRUMFIwUKBOoEyGSmh0dHA6Ly93d3cuZ3N0YXRpYy5j
b20vR29vZ2xlSW50ZXJuZXRBdXRob3JpdHkvR29vZ2xlSW50ZXJuZXRBdXRob3Jp
dHkuY3JsMGYGCCsGAQUFBwEBBFowWDBWBggrBgEFBQcwAoZKaHR0cDovL3d3dy5n
c3RhdGljLmNvbS9Hb29nbGVJbnRlcm5ldEF1dGhvcml0eS9Hb29nbGVJbnRlcm5l
dEF1dGhvcml0eS5jcnQwIQYJKwYBBAGCNxQCBBQeEgBXAGUAYgBTAGUAcgB2AGUA
cjANBgkqhkiG9w0BAQUFAAOBgQC+O+saZyuEutlgPpW71t9QlJOWpGikWdHuKxfM
Nl96+A7S7Qo3qodt90jrl6uhyuzV/apR++Rv8R+rRacvwY1R9yLh9k/oz/a8Qg/3
IhJRHIl3FlPuTO9+/zZ3/9GJZIcOeworqto4T31P/pvMqGbStCHYCNEuB2NakE2x
p3EwIA==
-----END CERTIFICATE-----
subject=/C=US/ST=California/L=Mountain View/O=Google Inc/CN=smtp.googlemail.com
issuer=/C=US/O=Google Inc/CN=Google Internet Authority
---
No client certificate CA names sent
---
SSL handshake has read 2078 bytes and written 444 bytes
---
New, TLSv1/SSLv3, Cipher is ECDHE-RSA-RC4-SHA
Server public key is 1024 bit
Secure Renegotiation IS supported
Compression: NONE
Expansion: NONE
SSL-Session:
    Protocol  : TLSv1.2
    Cipher    : ECDHE-RSA-RC4-SHA
    Session-ID: F7067B4D236E210867289471187790F3F3FEF94EABB59D2B9F2862DB3493850B
    Session-ID-ctx: 
    Master-Key: E565E778D3788196E050099A69E7B8C663D8B756E92B1F457764AE6711FE40D7C33E16C6F92669D870FFC506C832F092
    Key-Arg   : None
    PSK identity: None
    PSK identity hint: None
    SRP username: None
    TLS session ticket lifetime hint: 100800 (seconds)
    TLS session ticket:
    0000 - 57 5f a4 47 c6 63 ff 98-22 34 5c 45 c8 0b 08 b3   W_.G.c.."4\E....
    0010 - 47 f5 32 2c 36 71 3f 85-43 ed 8a 65 08 c3 f4 81   G.2,6q?.C..e....
    0020 - 41 4f bf 46 78 b6 c4 14-51 de c7 c7 64 10 ad 81   AO.Fx...Q...d...
    0030 - c3 4c 44 c3 85 b1 a6 55-8d 43 cf a5 00 93 1f 7c   .LD....U.C.....|
    0040 - 14 54 bc df 89 b7 08 60-08 63 b4 c0 fe 3a 42 2f   .T.....`.c...:B/
    0050 - 42 d9 8f 84 fc 48 e1 d1-00 ee 29 fc 7d 0d 03 3c   B....H....).}..<
    0060 - 79 a4 00 3c 5f 9d ad fe-d6 bf e9 6c d3 63 8a 9b   y..<_......l.c..
    0070 - 64 69 51 9c ca 2d e0 68-a5 18 a5 8a 55 d2 2b c6   diQ..-.h....U.+.
    0080 - e9 c7 37 5f 5c bb 13 21-f7 1e 65 d3 fd 77 8e 32   ..7_\..!..e..w.2
    0090 - 7a ed 8a fa                                       z...

    Start Time: 1350340253
    Timeout   : 300 (sec)
    Verify return code: 20 (unable to get local issuer certificate)
---
220 mx.google.com ESMTP qn3sm7498842igc.7
ehlo
250-mx.google.com at your service, [64.131.104.21]
250-SIZE 35882577
250-8BITMIME
250-AUTH LOGIN PLAIN XOAUTH XOAUTH2
250 ENHANCEDSTATUSCODES
auth login
334 VXNlcm5hbWU6
cmF0dGFyZEBoYXdrLmlpdC5lZHUK
334 UGFzc3dvcmQ6
PASSWORDHASHCENSORED
235 2.7.0 Accepted
help
214 2.0.0 http://www.google.com/search?btnI&q=R
