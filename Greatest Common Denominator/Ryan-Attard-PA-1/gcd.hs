import System.Environment
import Data.Int
main = do
    args <- getArgs
    (print $ ((argtoint(args))))
argtoint :: [String]->Integer
argtoint a = do
        mygcd (read (head a) :: Integer) (read (head (tail a)) :: Integer)


mygcd :: Integer ->Integer ->Integer
mygcd a b | (a==0 || b==0) = a+b
          | a==b = a
          | a>b = mygcd (a-b) b
          | otherwise=mygcd a (b-a)
