import Network.Pcap
import Text.Printf
import System.Process
main = do
--    spy <- openLive "eth0" 128 True (1)
--    spydump<-openDump spy "out.txt"
--    (pkthdr,pktdata)<-next spy
--    dump spydump (pkthdr) pktdat
    printf "Capturing next 2000 packets in background\n"
    runCommand "tcpdump -i eth0 -w dump.txt -c 2000 >> /dev/null 2>&1"
