module HPrimeGen (primegen, nextprime, nextprimegt,nextprimeuntil) where
import HPrimeCheck
import Control.Applicative
import System.IO.Unsafe
import System.Random

--Generate a random seed between the range of 2^n-1,
--since it needs to add up to 2^n digits
seedgen n = (randgenrange 0 ((2^(n-1))-(2^(n-2))))
randgenrange n m = unsafePerformIO $ randomRIO(n,m)

primegen :: Integer->Integer
primegen n =let seed=seedgen n in
             let testprime= ((2^(n-1))-1+seed) in
                --Always start with an odd, since all primes are odd
                case (mod testprime 2) of
                  0-> case primecheck (testprime+1) of
                      True -> (testprime+1)
                      False-> nextprime (testprime+1)
                  1->case primecheck  testprime of
                    True -> testprime
                    False-> nextprime testprime
nextprime :: Integer->Integer
nextprime a = case (primecheck a) of
                True -> a
                False-> case (a `mod` 2) of
                           1->nextprime (a+2)
                           0->nextprime (a+1)
nextprimegt :: Integer->Integer
nextprimegt a = nextprime (a+1)

nextprimeuntil base [] = nextprimeuntil base [1]
nextprimeuntil base (x:xs) = case (last (x:xs)) > base of
                            True-> ((init xs))
                            otherwise ->nextprimeuntil base (x:xs++[nextprimegt (last (x:xs))])
