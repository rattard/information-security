module HPrimeCheck (primecheck) where

primecheck :: Integer -> Bool
primecheck n = primecheckW n 3

primecheckW :: Integer -> Integer -> Bool
primecheckW n a= case a<(n-2) of
                True ->  case (primalityCheck n a) of
                            False -> False
                            True -> primecheckW n (2*a-1)
                False -> True

twotothektimesm :: Integral a => a -> (a,a)
twotothektimesm n = f 0 n
    where
        f k m
            | remainder == 1 = (k,m)
            | otherwise = f (k+1) q
            where (q,remainder) = quotRem m 2

primalityCheck :: Integer -> Integer -> Bool
primalityCheck n witness
    |witness >= n-1 =
        error $ "witness too big for prime"
    | witness <= 1  =
        error $ "witness too small for prime"
    | n < 3 = False
    | even n = False
    | base0 == 1 || base0 == (n-1) = True
    | otherwise = iter (tail base)
    where
        (k,m) = twotothektimesm (n-1)
        base0 = powMod n witness m
        base = take (fromIntegral k) $ iterate (squarethenmod n) base0
        iter [] = False
        iter (x:xs)
            | x == 1 = False
            | x == (n-1) = True
            | otherwise = iter xs

fastpow :: (Num a, Integral b) => (a->a->a) -> (a->a) -> a -> b -> a
fastpow _ _ _ 0 = 1
fastpow squareandmult sq x' n' = fun x' n' 1
    where
        fun x n y
            | n == 1 = x `squareandmult` y
            | r == 0 = fun xsquared q y
            | otherwise = fun xsquared q (x `squareandmult` y)
            where
                (q,r) = quotRem n 2
                xsquared = sq x

multthenmod :: Integral a => a -> a -> a -> a
multthenmod a b c = (b * c) `mod` a
squarethenmod :: Integral a => a -> a -> a
squarethenmod a b = (b * b) `rem` a

powMod :: Integral a => a -> a -> a -> a
powMod m = fastpow (multthenmod m) (squarethenmod m)
