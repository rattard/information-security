import System.Environment
main = do
    args <- getArgs
    putStrLn $ show  (argtoint(args))
argtoint a = do
        encrypt (read (head a) :: Integer) (read (head (tail a)) :: Integer) (read (head (tail (tail a))) :: Integer)
encrypt n e m = ((m^e) `mod` n)
